#!/usr/bin/env bash

# Backup script for MediaWiki instaillations using SQLite database

# Script assumes backup dir structure of:
# $backup_path
#  |- daily
#  |- weekly
#    |- $date
# Hardcoded rentention values: 7 days (1 week) for daily db backups, 28 days (~1 month) for full weekly backups

mw_path='/var/mediawiki'
backup_path='/var/backup'
weekly_db='waapt_wiki'
weekly_tarball='wikidata'

daily_backup() {
	cd $mw_path

	php maintenance/sqlite.php --backup-to $backup_path/daily/$(date -I).sqlite

	find $backup_path/daily -mtime 7 -type f -delete
}

weekly_backup() {

	daily_backup

	mkdir $backup_path/weekly/$(date -I)
	cd $mw_path

	tar zcvhf $backup_path/weekly/$(date -I)/$weekly_tarball.tgz ./mediawiki
	cp $backup_path/daily/$(date -I).sqlite $backup_path/weekly/$(date -I)/$weekly_db.sqlite

	cd $backup_path/weekly
	find . ! -path . -mtime 28 -type d -exec rm -rf {} +
}

case $1 in
	daily)
		daily_backup
	;;
	weekly)
		weekly_backup
	;;
	*)
	;;
esac
